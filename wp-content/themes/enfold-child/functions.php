<?php

/*
* Add your own functions here. You can also copy some of the theme functions into this file. 
* Wordpress will use those functions instead of the original functions then.
*/

if ( !function_exists('avia_woocommerce_frontend_search_params') )
{
    add_action('woocommerce_before_shop_loop', 'avia_woocommerce_frontend_search_params', 20);

    function avia_woocommerce_frontend_search_params()
    {

        if ( is_product_category() )
        {
            $output = "<div class='shopping-page-title'>";
            $output .= "<h1>";

            $category = single_cat_title('', false);
            $output .= $category;

            $output .= "</h1>";
            $output .= "</div>";

            echo $output;
        }
    }
}

add_filter('woocommerce_product_tabs', 'woo_remove_product_tabs', 98);
function woo_remove_product_tabs($tabs)
{
    unset($tabs['additional_information']);    // Remove the additional information tab

    return $tabs;

}

add_filter('woocommerce_product_tabs', 'woo_tehnical_specs_tab');
function woo_tehnical_specs_tab($tabs)
{

    // Adds the new tab

    $tabs['tehnical_specs_tab'] = array(
        'title' => __('Specificaţii tehnice', 'woocommerce'),
        'priority' => 50,
        'callback' => 'woo_new_product_tab_content'
    );

    return $tabs;

}

function woo_new_product_tab_content()
{
    global $product;

    $_tehnical_specs = get_post_meta($product->id, '_tehnical_specs', true);

    if ( $_tehnical_specs )
    {
        echo nl2br($_tehnical_specs);
    }
}

add_action('woocommerce_product_options_general_product_data', 'woo_add_custom_general_fields');
function woo_add_custom_general_fields()
{

    global $post;

    $specs = get_post_meta($post->ID, '_tehnical_specs', true);

    if ( !$specs )
    {
        $specs = '';
    }

    echo '<div class="options_group">';

    $args = array(
        'wpautop' => true, // enable auto paragraph?
        'media_buttons' => false, // show media buttons?
        'textarea_rows' => get_option('default_post_edit_rows', 10), // This is equivalent to rows="" in HTML
        'tabindex' => '',
        'editor_css' => '', //  additional styles for Visual and Text editor,
        'editor_class' => '', // sdditional classes to be added to the editor
        'teeny' => true, // show minimal editor
        'dfw' => false, // replace the default fullscreen with DFW
        'tinymce' => array(
            // Items for the Visual Tab
            'toolbar1'=> 'bold,italic,underline,bullist,numlist,link,unlink,forecolor,undo,redo,',
        ),
        'quicktags' => array(
            // Items for the Text Tab
            'buttons' => 'strong,em,underline,ul,ol,li,link,code'
        )
    );

    wp_editor( $specs, '_tehnical_specs', $args );

    echo '</div>';

}

// Save Fields
add_action('woocommerce_process_product_meta', 'woo_add_custom_general_fields_save');

function woo_add_custom_general_fields_save($post_id)
{
    // Text Field
    $_tehnical_specs = $_POST['_tehnical_specs'];

    if ( !empty($_tehnical_specs) )
    {
        update_post_meta($post_id, '_tehnical_specs', ($_tehnical_specs));
    }
}

#
# display tabs and related items after the summary wrapper
#
function enfold_woo_tabs_setup() {
    remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 1 );
    add_action('woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10);
}
add_action( 'after_setup_theme', 'enfold_woo_tabs_setup', 20 );

// remove add to cart option
remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart');
remove_action( 'woocommerce_variable_add_to_cart', 'woocommerce_variable_add_to_cart', 30 );
remove_action( 'woocommerce_single_variation', 'woocommerce_single_variation_add_to_cart_button', 20 );
add_action( 'woocommerce_variable_add_to_cart', 'woocommerce_variable_add_to_cart_enfold_child', 30 );

/**
 * Add cross sells products on the product page
 */
add_action( 'woocommerce_after_single_product_summary', 'enfold_woocommerce_output_crossels', 12);

function enfold_woocommerce_output_crossels($items = false, $columns = false)
{
    global $avia_config;

    if(!$items) 	$items 	 = $avia_config['shop_single_column_items'];
    if(!$columns) 	$columns = $avia_config['shop_single_column'];

    $output = "";

    $crosssells_ids = get_post_meta( get_the_ID(), '_crosssell_ids',true);

    $crosssells = wc_products_array_orderby( array_filter( array_map( 'wc_get_product', $crosssells_ids ), 'wc_products_array_filter_visible' ) );
    $crosssells = $items > 0 ? array_slice( $crosssells, 0, $items ) : $crosssells;

    ob_start();
    wc_get_template( 'single-product/cross-sells.php', array(
        'cross_sells' => $crosssells,

        // Not used now, but used in previous version of up-sells.php.
        'posts_per_page' => $items,
        //'orderby'        => $orderby,
        'columns'        => $columns,
    ), '' );

    $content = ob_get_clean();
    if($content)
    {
        $output .= "<div class='product_column product_column_".$columns."'>";
        //$output .= "<h3>".(__('You may also like', 'avia_framework'))."</h3>";
        $output .= $content;
        $output .= "</div>";
    }

    echo $output;
}

if ( ! function_exists( 'woocommerce_variable_add_to_cart_enfold_child' ) ) {

    /**
     * Output the variable product add to cart area.
     *
     * @subpackage	Product
     */
    function woocommerce_variable_add_to_cart_enfold_child() {
        global $product;

        $attributes_labels = [
            'attribute_color' => __( 'Culori disponibile', 'woocommerce' ),
        ];

        // Enqueue variation scripts.
        wp_enqueue_script( 'wc-add-to-cart-variation' );

        // Get Available variations?
        $get_variations = count( $product->get_children() ) <= apply_filters( 'woocommerce_ajax_variation_threshold', 30, $product );

        $variations = $product->get_available_variations();

        $attributes = [];

        if($variations) {
            foreach($variations as $variation)
            {
                $group = '';
                $label = '';

                foreach ($variation['attributes'] as $key => $value)
                {

                    if(isset($attributes_labels[$key]) && $value) {
                        $group .= $key;

                        if($key == 'attribute_color') {
                            $label = (!$label ? $attributes_labels[$key] : $label) . ' ';
                        } else {
                            $label = $attributes_labels[$key] . ' ' . $value . ' ' . $label;
                            $label = str_replace($attributes_labels['attribute_color'], '', $label);
                        }
                    }
                    else if ( $value )
                    {
                        $group .= $value;
                        $label.= ' ' . $value;
                    }
                }

                if(!isset($attributes[$group])) {
                    $attributes[$group]['label'] = $label;
                }

                $attributes[$group]['values'][] = $variation;
            }
        }

        // Load the template.
        wc_get_template( 'single-product/add-to-cart/variable.php', array(
            'available_variations' => $get_variations ? $product->get_available_variations() : false,
            'attributes'           => $attributes,
            'selected_attributes'  => $product->get_default_attributes(),
        ) );
    }
}

function bbloomer_remove_product_page_sku( $enabled ) {
    if ( !is_admin() && is_product() ) {
        return false; // only if its frontend and single product
    }
 
    return $enabled;
}
 
add_filter( 'wc_product_sku_enabled', 'bbloomer_remove_product_page_sku' );