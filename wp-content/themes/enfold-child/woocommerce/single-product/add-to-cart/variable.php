<?php
/**
 * Variable product add to cart
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/add-to-cart/variable.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.0.0
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

global $product;

$attribute_keys = array_keys( $attributes );

do_action( 'woocommerce_before_add_to_cart_form' ); ?>

<?php do_action( 'woocommerce_before_variations_form' ); ?>

<?php if ( empty( $available_variations ) && false !== $available_variations ) : ?>
    <p class="stock out-of-stock"><?php _e( 'This product is currently out of stock and unavailable.', 'woocommerce' ); ?></p>
<?php else : ?>
        <?php foreach ( $attributes as $key => $group) : ?>
            <div class="flex_column product-variations">
            <h3><?php echo esc_html( apply_filters( 'woocommerce_product_description_heading', $group['label'] ) ); ?></h3>
            <?php foreach ( $group['values'] as $attribute_name => $options ) : ?>
            <div class="av_one_half product-variation">
                <div class="av_one_half variation-image">
                    <?php echo '<img src="' . esc_url( $options['image']['src'] ) . '" alt="' . esc_attr( $options['image']['alt'] ) . '" width="' . esc_attr( $options['image']['src_w'] ) . '" height="' . esc_attr(  $options['image']['src_h'] ) . '" />'; ?>
                </div>
                <div class="av_one_half variation-text">
                    <p><?php echo $options['attributes']['attribute_color'] ?></p>
                    <p><b><?php echo $options['display_price'] . ' lei' ?></b></p>
                    <p><small><?php echo $options['variation_description'] ?></small></p>
                </div>
            </div>
            <?php endforeach; ?>
            <div class="attributes-variation-end"></div>
            </div>
        <?php endforeach; ?>

    <?php do_action( 'woocommerce_before_add_to_cart_button' ); ?>

    <div class="single_variation_wrap">
        <?php
            /**
             * woocommerce_before_single_variation Hook.
             */
            do_action( 'woocommerce_before_single_variation' );

            /**
             * woocommerce_single_variation hook. Used to output the cart button and placeholder for variation data.
             * @since 2.4.0
             * @hooked woocommerce_single_variation - 10 Empty div for variation data.
             * @hooked woocommerce_single_variation_add_to_cart_button - 20 Qty and cart button.
             */
            do_action( 'woocommerce_single_variation' );

            /**
             * woocommerce_after_single_variation Hook.
             */
            do_action( 'woocommerce_after_single_variation' );
        ?>
    </div>

    <?php do_action( 'woocommerce_after_add_to_cart_button' ); ?>
<?php endif; ?>

<?php do_action( 'woocommerce_after_variations_form' ); ?>


<?php
do_action( 'woocommerce_after_add_to_cart_form' );
